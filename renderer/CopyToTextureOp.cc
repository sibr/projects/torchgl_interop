#define NOSIBR 0
#include "CopyToTextureOp.h"

#include <cuda_gl_interop.h>

#define CUDA_CHECK(ans) \
  { gpuAssert((ans), __FILE__, __LINE__); }

inline void gpuAssert(cudaError_t code, const char* file, int line,
	bool abort = true) {
	if (code != cudaSuccess) {
		fprintf(stderr, "GPUassert: %s %s %d\n", cudaGetErrorString(code), file,
			line);
		if (abort) exit(code);
	}
}
#define CUDA_CHECK_ERROR CUDA_CHECK(cudaPeekAtLastError())

//------------------------------------------------------------------------------

CopyToTextureOp::CopyToTextureOp(int texId)
	{
	texture_id_ = texId;
	std::cout << "Texture Id " << texture_id_ << std::endl;
	_NCHW = true;

	cudaGraphicsGLRegisterImage(&cudaTexture_, texture_id_, GL_TEXTURE_2D,
		cudaGraphicsMapFlagsWriteDiscard);
}

CopyToTextureOp::~CopyToTextureOp() {
	cudaGraphicsUnregisterResource(cudaTexture_);
	CUDA_CHECK_ERROR
}

void CopyToTextureOp::Compute(torch::Tensor & t) {

	//std::cout << "Computing output" << std::endl;

	cudaDeviceSynchronize();

	cudaGraphicsMapResources(1, &cudaTexture_);

	cudaArray_t texture_array;
	cudaGraphicsSubResourceGetMappedArray(&texture_array, cudaTexture_, 0, 0);

	cudaResourceDesc res_desc;
	memset(&res_desc, 0, sizeof(res_desc));
	res_desc.resType = cudaResourceTypeArray;
	res_desc.res.array.array = texture_array;

	cudaSurfaceObject_t out_surface;
	cudaCreateSurfaceObject(&out_surface, &res_desc);

	//std::cout << t.sizes() << std::endl;

	CopyToTexture(t.sizes() , (float *) t.view(-1).data_ptr() , out_surface, _NCHW);

	cudaDestroySurfaceObject(out_surface);

	cudaGraphicsUnmapResources(1, &cudaTexture_);
}
