#include "TextureInputOp.h"

#include <cuda_gl_interop.h>
#include "core/graphics/Window.hpp"

#define CUDA_CHECK(ans) \
  { gpuAssert((ans), __FILE__, __LINE__); }

inline void gpuAssert(cudaError_t code, const char* file, int line,
	bool abort = true) {
	if (code != cudaSuccess) {
		fprintf(stderr, "GPUassert: %s %s %d\n", cudaGetErrorString(code), file,
			line);
		if (abort) exit(code);
	}
}
#define CUDA_CHECK_ERROR CUDA_CHECK(cudaPeekAtLastError())

//------------------------------------------------------------------------------

TextureInputOp::TextureInputOp(sibr::Window::Ptr& win, std::vector<GLuint> texId, std::vector<int> channels)
{
	window_ = win;

	textureIds_ = texId;
	textureChs_ = channels;

	numInputs_ = textureIds_.size();
	_NCHW = true;

	window_->makeContextCurrent();
	cudaTextures_.resize(numInputs_);
	for (size_t i = 0; i < numInputs_; ++i) {
		cudaGraphicsGLRegisterImage(&cudaTextures_[i], textureIds_[i],
			GL_TEXTURE_2D, cudaGraphicsMapFlagsReadOnly);
	}
	//window_->makeContextNull();
}

TextureInputOp::~TextureInputOp() {
	window_->makeContextCurrent();
	for (size_t i = 0; i < numInputs_; ++i) {
		cudaGraphicsUnregisterResource(cudaTextures_[i]);
	}
	//window_->makeContextNull();
	CUDA_CHECK_ERROR
}

void TextureInputOp::Compute(torch::Tensor& t) {
	//    const auto stream =
	//        static_cast<stream_executor::cuda::CUDAStream*>(
	//            context->op_device_context()->stream()->implementation())
	//            ->cuda_stream();
	//    LOG(INFO) << "::" << stream << std::endl;

	cudaDeviceSynchronize();

	if (window_->getContextCurrent() != window_->GLFW()) {
		window_->makeContextCurrent();
	}

	cudaGraphicsMapResources(numInputs_, cudaTextures_.data());  //, stream);

	std::vector<cudaTextureObject_t> in_textures(numInputs_);

	for (size_t i = 0; i < numInputs_; ++i) {
		cudaArray_t texture_array;
		cudaGraphicsSubResourceGetMappedArray(&texture_array, cudaTextures_[i], 0,
			0);

		cudaResourceDesc res_desc;
		memset(&res_desc, 0, sizeof(res_desc));
		res_desc.resType = cudaResourceTypeArray;
		res_desc.res.array.array = texture_array;

		cudaTextureDesc texDesc;
		memset(&texDesc, 0, sizeof(texDesc));
		texDesc.addressMode[0] = cudaAddressModeBorder;
		texDesc.addressMode[1] = cudaAddressModeBorder;
		texDesc.addressMode[2] = cudaAddressModeBorder;
		texDesc.filterMode = cudaFilterModePoint;
		//texDesc.readMode = cudaReadModeNormalizedFloat;  // converts to [0., 1.]
		texDesc.readMode = cudaReadModeElementType;

		cudaCreateTextureObject(&in_textures[i], &res_desc, &texDesc, nullptr);
	}

	CopyToTensor(t.sizes(), textureChs_, in_textures,
		(float*)t.view(-1).data_ptr(), _NCHW);

	for (size_t i = 0; i < numInputs_; ++i) {
		cudaDestroyTextureObject(in_textures[i]);
	}
	
	cudaGraphicsUnmapResources(numInputs_, cudaTextures_.data());  //, stream);

	//window_->makeContextNull();
}
