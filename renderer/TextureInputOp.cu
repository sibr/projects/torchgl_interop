#include "TextureInputOp.h"

#include <vector>
#include <numeric>

__constant__ cudaTextureObject_t c_RGBAInputs[TextureInputOp::MAX_NUM_INPUTS];
__constant__ int	c_channels[128];
__constant__ int	c_channels_total[1];
__constant__ int	c_channelsCumSum[128];

__global__ void TextureInputOp_CopyToTexture_kernel(const size_t width,
	const size_t height,
	float* rgb) {
	cudaTextureObject_t t = c_RGBAInputs[blockIdx.z];
	const int channels = c_channels[blockIdx.z];
	const int channelsCumSum = c_channelsCumSum[blockIdx.z];
	const int channelsTotal = c_channels_total[0];

	//if (blockIdx.y == 0 && blockIdx.x == 0 && threadIdx.x == 0)
	//	printf("Hello: %d, %d ,%d", c_channels_total[0], c_channels[0], c_channels[1]);

	size_t y = blockIdx.y * blockDim.y + threadIdx.y;

	for (; y < height; y += blockDim.y * gridDim.y) {
		size_t x = blockIdx.x * blockDim.x + threadIdx.x;

		for (; x < width; x += blockDim.x * gridDim.x) {

			for (size_t c = 0; c < channels; ++c) {
				//const size_t offset = ((channelsCumSum ) * height + y* channels) * width + x* channels +c;
				const size_t offset = (y * width + x) * channelsTotal + (channelsCumSum + c);
				const float4 value = tex2D<float4>(t, x, y);  // in [0, 1]

				rgb[offset] = *(reinterpret_cast<const float*>(&value) + c);
			}
		}
	}
}

__global__ void TextureInputOp_CopyToTextureNCHW_kernel(const size_t width,
	const size_t height,
	float* rgb) {
	cudaTextureObject_t t = c_RGBAInputs[blockIdx.z];
	const int channels = c_channels[blockIdx.z];
	const int channelsCumSum = c_channelsCumSum[blockIdx.z];
	const int channelsTotal = c_channels_total[0];

	//if (blockIdx.y == 0 && blockIdx.x == 0 && threadIdx.x == 0)
	//	printf("Hello: %d, %d ,%d", c_channels_total[0], c_channels[0], c_channels[1]);
	for (size_t c = 0; c < channels; ++c) {
		size_t y = blockIdx.y * blockDim.y + threadIdx.y;

		for (; y < height; y += blockDim.y * gridDim.y) {
			size_t x = blockIdx.x * blockDim.x + threadIdx.x;

			for (; x < width; x += blockDim.x * gridDim.x) {

				//const size_t offset = ((channelsCumSum ) * height + y* channels) * width + x* channels +c;
				const size_t offset = ((channelsCumSum + c)* height + y)* width + x ;
				const float4 value = tex2D<float4>(t, x, y);  // in [0, 1]

				rgb[offset] = *(reinterpret_cast<const float*>(&value) + c);
			}
		}
	}
}

void TextureInputOp::CopyToTensor(
	c10::IntArrayRef shape,
      const std::vector<int>& channels,
      const std::vector<cudaTextureObject_t>& in_textures,
      float* out_tensor,
	  bool NCHW) {

	size_t width;
	size_t height;

	int shiftDim = shape.size() - 4;


	if (NCHW) {
		width = shape.at(shiftDim+3);
		height = shape.at(shiftDim+2);
	}
	else {
		width = shape.at(shiftDim+2);
		height = shape.at(shiftDim+1);
	}

	const dim3 block_dim(256, 1, 1);
	const dim3 grid_dim((width + block_dim.x - 1) / block_dim.x,
		(height + block_dim.y - 1) / block_dim.y,
		in_textures.size());

	std::vector<int> channelsCumSum(channels.size());
	std::partial_sum(channels.begin(), channels.end() - 1, channelsCumSum.begin() + 1);
	const int channel_total = std::accumulate(channels.begin(), channels.end(), 0);

	cudaMemcpyToSymbol(c_RGBAInputs, in_textures.data(),
		in_textures.size() * sizeof(cudaTextureObject_t));

	cudaMemcpyToSymbol(c_channels, &channels[0], channels.size() * sizeof(int), 0);
	cudaMemcpyToSymbol(c_channels_total, &channel_total, sizeof(int), 0);
	cudaMemcpyToSymbol(c_channelsCumSum, &channelsCumSum[0], channels.size() * sizeof(int), 0);

	// TODO (True): stream
	if (NCHW) {
		TextureInputOp_CopyToTextureNCHW_kernel
			<< <grid_dim, block_dim >> > (width, height, out_tensor);
	}
	else {

		TextureInputOp_CopyToTexture_kernel
			<< <grid_dim, block_dim >> > (width, height, out_tensor);
	}
}
