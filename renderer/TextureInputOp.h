#ifndef TEXTURE_INPUTS_OP_H_
#define TEXTURE_INPUTS_OP_H_

#include <vector>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <cuda_runtime.h>
#include "torch/torch.h"
#include "core/graphics/Window.hpp"

class TextureInputOp {
 public:
  static const size_t MAX_NUM_INPUTS = 64;

  TextureInputOp(sibr::Window::Ptr& win, std::vector<GLuint> texId, std::vector<int> channels);

  ~TextureInputOp();

  void Compute(torch::Tensor & t);

  static void CopyToTensor(
	  c10::IntArrayRef shape,
      const std::vector<int>& channels,
      const std::vector<cudaTextureObject_t>& in_textures,
      float* out_tensor,
	  bool NCHW);

 private:
  std::vector<GLuint> textureIds_;
  std::vector<int> textureChs_;
  std::vector<cudaGraphicsResource_t> cudaTextures_;
  bool _NCHW;
  std::shared_ptr<sibr::Window> window_;
  size_t numInputs_;
};

#endif  // TEXTURE_INPUTS_OP_H_
