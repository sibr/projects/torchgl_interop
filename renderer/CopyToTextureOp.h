#ifndef COPY_TO_TEXTURE_OP_H_
#define COPY_TO_TEXTURE_OP_H_

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <cuda_runtime.h>

#include "torch/torch.h"
#include "core/graphics/Window.hpp"

class CopyToTextureOp {
 public:
  explicit CopyToTextureOp(int texId);

  ~CopyToTextureOp();

  void Compute(torch::Tensor & t);

  static void CopyToTexture(
	  c10::IntArrayRef shape,
	  const float* in_tensor,
      cudaSurfaceObject_t out_texture,
		bool NCHW);

 private:
  GLuint texture_id_;
  bool _NCHW;
  std::shared_ptr<sibr::Window> window_;
  cudaGraphicsResource_t cudaTexture_;
};

#endif // COPY_TO_TEXTURE_OP_H_
