// partly adapted from:
// https://github.com/tensorflow/tensorflow/blob/master/tensorflow/examples/label_image/main.cc
// see also: https://www.tensorflow.org/versions/master/api_guides/cc/guide

#include "torchgl_interop.h"

#include <algorithm>
#include <chrono>
#include <fstream>
#include <sstream>
#include <vector>

#include <cassert>

#include <opencv2/opencv.hpp>

#include <Eigen/Core>

#include <torch/script.h> // One-stop header.
#include <c10/cuda/CUDACachingAllocator.h>

sibr::TORCH_GL::TORCH_GL(
	std::string model_path,
	std::vector<std::shared_ptr<TexInfo>>& input_textures,
	std::vector<std::vector<int>> input_texture_mapping,
	std::vector<std::vector<int>> input_texture_channels_vec,
	std::vector<std::shared_ptr<TexInfo>>& output_texture,
	sibr::Window::Ptr window,
	std::vector<std::shared_ptr<at::Tensor>>& preAllocTensors,
	std::vector<torch::jit::IValue>& inputNet
) : _input_textures(input_textures),
_input_texture_mapping(input_texture_mapping),
_input_texture_channels_vec(input_texture_channels_vec),
_output_texture(output_texture),
_window(window),
_inputNet(inputNet)
{

	_inTensors = preAllocTensors;

	torch::NoGradGuard no_grad_guard;

	try {
		// Deserialize the ScriptModule from a file using torch::jit::load().
		_model = torch::jit::load(model_path);
		_model.to(at::kCUDA);
		_output = std::make_shared<at::Tensor>(torch::ones({ 1,3,output_texture[0]->h,output_texture[0]->w }).cuda());
		*_output = _model.forward(_inputNet).toTensor();
		std::cout << "Successfuly loaded model" << std::endl;
	}
	catch (const c10::Error& e) {
		std::cerr << e.msg_stack() << std::endl;
		std::cerr << "error loading the model\n";
		SIBR_ERR;
	}


	for (auto& ot : _output_texture) {
		_cpOps.push_back(
			std::make_shared<CopyToTextureOp>
			(ot->handle)
		);
		std::cout << "handle" << ot->handle << std::endl;
	};

	//Register the ops for input
	for (int it = 0; it < _input_texture_mapping.size(); it++) {
		std::vector<GLuint> texs;
		for (auto& id_it : _input_texture_mapping[it]) {
			std::cout << id_it << " : " << _input_textures[id_it]->handle << std::endl;
			texs.push_back(_input_textures[id_it]->handle);
		}

		_tiOps.push_back(
			std::make_shared<TextureInputOp>(
				_window,
				texs,
				_input_texture_channels_vec[it])
		);
	}

	_cpOps[0]->Compute(*_output);

};


void sibr::TORCH_GL::run(std::vector<at::Tensor>& addTensor) {



	auto start = std::chrono::high_resolution_clock::now();
	for (int tiOp_id = 0; tiOp_id < _tiOps.size(); tiOp_id++) {
		_tiOps[tiOp_id]->Compute(*_inTensors[tiOp_id]);
	}
	std::vector<torch::jit::IValue> run_inputNet = _inputNet;
	run_inputNet.insert(run_inputNet.end(), addTensor.begin(), addTensor.end());
	auto timeNowNet = std::chrono::steady_clock::now();
	float deltaTimeNet = std::chrono::duration<float>(timeNowNet - start).count();
	start = timeNowNet;
	std::cout << "renderCopy " << deltaTimeNet << std::endl;
	torch::NoGradGuard no_grad_guard; //Very important avoid memory leak, if not used, gradients are store and accumulate on gpu
	*_output = _model.forward(run_inputNet).toTensor();

	timeNowNet = std::chrono::steady_clock::now();
	deltaTimeNet = std::chrono::duration<float>(timeNowNet - start).count();
	std::cout << "renderFwd " << deltaTimeNet << std::endl;

	//_window->makeContextCurrent();
	_cpOps[0]->Compute(*_output);

}

void sibr::TORCH_GL::loadModel(std::string model_path) {
	torch::NoGradGuard no_grad_guard;

	try {
		// Deserialize the ScriptModule from a file using torch::jit::load().
		_model = torch::jit::load(model_path);
		_model.to(at::kCUDA);
		std::cout << "Successfuly loaded model" << std::endl;
	}
	catch (const c10::Error& e) {
		std::cerr << e.msg_stack() << std::endl;
		std::cerr << "error loading the model\n";
		SIBR_ERR;
	}

}

torch::Tensor sibr::matrix4fToTensor(const Matrix4f mat) {
	float* raw_data = new float[16];
	Mat44fR::Map(raw_data) = mat;
	return torch::from_blob(raw_data, { 4,4 }, torch::kFloat).clone();
}

torch::Tensor sibr::imToTensor(const sibr::ImageL32F& im) {
	torch::Tensor ts = torch::zeros({ 1, im.h(), im.w(), 1 }, torch::kF32);
	std::memcpy(ts.data_ptr(), im.toOpenCV().data, ts.numel() * sizeof(float));
	ts = ts.permute({ 0, 3, 1, 2 }).contiguous();
	return ts;
};

torch::Tensor sibr::imToTensor(const sibr::ImageRGB& im) {
	torch::Tensor ts = torch::zeros({ 1, im.h(), im.w(), 3 }, torch::kByte);
	std::memcpy(ts.data_ptr(), im.toOpenCV().data, ts.numel() * sizeof(char));
	ts = ts.permute({ 0, 3, 1, 2 }).contiguous();// .cuda();
	return ts;
};

torch::Tensor sibr::imToTensor(const sibr::ImageRGB32F& im) {
	torch::Tensor ts = torch::zeros({ 1, im.h(), im.w(), 3 }, torch::kF32);
	std::memcpy(ts.data_ptr(), im.toOpenCV().data, ts.numel() * sizeof(float));
	ts = ts.permute({ 0, 3, 1, 2 }).contiguous();// .cuda();
	return ts;
};

torch::Tensor sibr::cvMatToTensor(const cv::Mat& im) {
	cv::Mat imCpy = im.clone();
	torch::Tensor ts = torch::zeros({ 1, imCpy.rows, imCpy.cols, 3 }, torch::kF32);
	std::memcpy(ts.data_ptr(), imCpy.data, ts.numel() * sizeof(float));
	ts = ts.permute({ 0, 3, 1, 2 });// .cuda();
	return ts;
};

sibr::ImageRGB32F sibr::tensorToIm(const torch::Tensor& ts) {
	std::cout << ts.sizes() << std::endl;
	sibr::ImageRGB32F im(ts.sizes()[3], ts.sizes()[2]);
	torch::Tensor tsCpy = ts.permute({ 0,2,3,1 }).cpu();
	std::memcpy((void*)im.toOpenCVnonConst().data, tsCpy.data_ptr(), tsCpy.numel() * sizeof(float));
	return im;
};

void sibr::saveTensor(const torch::Tensor& ts, const std::string& filepath) {
	auto bytes = torch::pickle_save(ts);
	std::ofstream fout(filepath, std::ios::out | std::ios::binary);
	fout.write(bytes.data(), bytes.size());
	fout.close();
}

torch::Tensor sibr::getWorld2ProjMat(const sibr::Camera::Ptr cam) {
	//return matrix4fToTensor(cam->viewproj());
	return torch::matmul(getP3DWorld2ViewMat(cam), getP3DProjMat(cam));
}
torch::Tensor sibr::getP3DProjMat(const sibr::Camera::Ptr cam) {
	// Convert Eigen To Tensor
	torch::Tensor proj = matrix4fToTensor(cam->proj());
	auto a = proj.accessor<float, 2 >();
	a[2][2] = cam->zfar() / (cam->zfar() - cam->znear());
	a[3][2] = -(cam->zfar() * cam->znear()) / (cam->zfar() - cam->znear());
	a[2][3] = 1.0;
	return proj;
}
torch::Tensor sibr::getP3DWorld2ViewMat(const sibr::Camera::Ptr cam) {
	// Convert Eigen to tesnor.
	torch::Tensor viewGL = matrix4fToTensor(cam->view());
	// Invert y,z axis
	return torch::matmul(viewGL, torch::tensor({ {1.0, 0.0, 0.0, 0.0},
												 {0.0,-1.0, 0.0, 0.0},
												 {0.0, 0.0,-1.0, 0.0},
												 {0.0, 0.0, 0.0, 1.0 } }));
}