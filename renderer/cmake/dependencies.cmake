find_package(CUDA REQUIRED)
link_directories(${CUDA_TOOLKIT_ROOT_DIR}/bin)

sibr_addlibrary(
    NAME libtorch
    MSVC11 "https://repo-sam.inria.fr/fungraph/dependencies/sibr/~0.9/libtorch.7z"
    MSVC14 "https://repo-sam.inria.fr/fungraph/dependencies/sibr/~0.9/libtorch-1.5.0.7z"
    SET CHECK_CACHED_VAR torch_DIR PATH "share/cmake/Torch"
)

# You need to tell TorchConfig which arhcitecture to use if it's not detectable
if((NOT (DEFINED CUDAARCHS)) AND (DEFINED ENV{CUDAARCHS}))
  set(TORCH_CUDA_ARCH_LIST "$ENV{CUDAARCHS}")
elseif(DEFINED CUDAARCHS)
  set(TORCH_CUDA_ARCH_LIST "${CUDAARCHS}")
endif()

find_package(torch REQUIRED)