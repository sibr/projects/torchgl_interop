#ifndef TORCHGL_INTEROP_H
#define TORCHGL_INTEROP_H

#include <memory>
#include <vector>

#include "core/graphics/Texture.hpp"
#include "core/graphics/Image.hpp"
#include <GL/glew.h>

#include "core/graphics/Window.hpp"
#include "core/graphics/Camera.hpp"

#include <cuda_gl_interop.h>
#include "torch/torch.h"

#include "TextureInputOp.h"
#include "CopyToTextureOp.h"

namespace sibr
{

	struct TexInfo {
		GLuint handle;
		int w;
		int h;

		template<typename T_Type, unsigned int T_NumComp> TexInfo(std::shared_ptr<RenderTarget<T_Type,T_NumComp>> t, int targetId=0) : handle(t->handle(targetId)), w(t->w()), h(t->h()) {};
		template<typename T_Type, unsigned int T_NumComp> TexInfo(std::shared_ptr<Texture2D<T_Type,T_NumComp>> t) : handle(t->handle()), w(t->w()), h(t->h()) {};
		TexInfo() : handle(-1), w(-1), h(-1) {};

	};

	class TORCH_GL
	{

	public:
		SIBR_CLASS_PTR(TORCH_GL);
		// Vector containing the input texture info
		std::vector<std::shared_ptr<TexInfo>>&_input_textures;
		std::vector<std::vector<int>> _input_texture_mapping;
		std::vector<std::vector<int>> _input_texture_channels_vec;
		std::vector<std::shared_ptr<TexInfo>>& _output_texture;

		torch::jit::script::Module _model;

		std::vector<std::shared_ptr<TextureInputOp>> _tiOps;
		std::vector<std::shared_ptr<CopyToTextureOp>> _cpOps;

		std::vector<torch::jit::IValue> _inputNet;
		std::vector<std::shared_ptr<at::Tensor>> _inTensors;
		std::shared_ptr<at::Tensor> _output;
		
		sibr::Window::Ptr _window;

		TORCH_GL(
			std::string model_file_name,
		 	std::vector<std::shared_ptr<TexInfo>>& input_textures,
			std::vector<std::vector<int>> input_texture_mapping,
			std::vector<std::vector<int>> input_texture_channels_vec,
			std::vector<std::shared_ptr<TexInfo>>& output_texture,
			sibr::Window::Ptr window,
			std::vector<std::shared_ptr<at::Tensor>> & preAllocTensors,
			std::vector<torch::jit::IValue>& inputNet
		);

		void run(std::vector<at::Tensor>& addTensor);
		
		void loadModel(std::string filepath);

	private:

		// Check if a string starts with another string.
		inline bool StartsWith(const std::string& str, const std::string& prefix) {
			return (str.size() >= prefix.size()) &&
				!str.compare(0, prefix.size(), prefix);
		};

		// Check if a string ends with another string.
		inline bool EndsWith(const std::string& str, const std::string& suffix) {
			return (str.size() >= suffix.size()) &&
				!str.compare(str.size() - suffix.size(), suffix.size(), suffix);
		};
		
	};
	
	torch::Tensor getWorld2ProjMat(const sibr::Camera::Ptr cam);
	torch::Tensor getP3DProjMat(const sibr::Camera::Ptr cam);
	torch::Tensor getP3DWorld2ViewMat(const sibr::Camera::Ptr cam);
	
	typedef Eigen::Matrix<float, 4, 4, Eigen::ColMajor> Mat44fR;
	torch::Tensor matrix4fToTensor(const Matrix4f mat);
	
	torch::Tensor imToTensor(const sibr::ImageRGB& im);
	torch::Tensor imToTensor(const sibr::ImageL32F& im);
	torch::Tensor imToTensor(const sibr::ImageRGB32F& im);

	torch::Tensor cvMatToTensor(const cv::Mat& im);

	sibr::ImageRGB32F tensorToIm(const torch::Tensor& ts);

	void saveTensor(const torch::Tensor& ts, const std::string& filepath);
	
}

#endif
